const router = require('express').Router();
const config = require('../../config/config');
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const uuid = require('uuid');

mapper.createMapper(['./mappers/client.xml', './mappers/file.xml']);

//Request to upload file.s
router.post('/', function (req, res, next) {
  var token = req.headers.baerer;
  var files = _.get(req, 'file') ? [_.get(req, 'file')] : _.get(req, 'files');

  if (!token || !files) {
    next(errorService.BAD_REQUEST_PARAMETERS);
  } else {
    jwt.verify(token, req.app.get('superSecret'), (err) => {
      if (err) {
        next(errorService.FORBIDDEN);
      } else {
        var clientQuery = mapper.getStatement('Client', 'select', { token: token }, db.conf);
        db.query(clientQuery, (err, clientDb) => {
          if (err) {
            next(errorService.UNEXPECTED_ERROR);
          } else if (clientDb.length == 0) {
              next(errorService.FORBIDDEN);
          } else {
            clientDb = clientDb.pop();

            var filesToInsert = [];
            _.forEach(files, (file) => {
              var newFile = {
                id: uuid.v1(),
                clientId: clientDb.id,
                originalName: file.originalname,
                mimeType: file.mimetype,
              };

              filesToInsert.push(newFile);
            });

            var fileQuery = mapper.getStatement('File', 'insertList', { files: filesToInsert });
            db.query(fileQuery, (err) => {
              if (err) {
                next(errorService.UNEXPECTED_ERROR);
              } else {
                res.json({
                  success: true,
                  data: _.map(filesToInsert, file => file.id)
                });
              }
            });
          }
        });
      }
    });
  }
});

module.exports = router;