const router = require('express').Router({ mergeParams: true });
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const uuid = require('uuid');

mapper.createMapper(['./mappers/client.xml', './mappers/room.xml', './mappers/file.xml', './mappers/booking.xml']);

// Request to get book a room
router.post('/', function (req, res, next) {
  var token = req.headers.baerer;
  var roomId = _.get(req.params, 'id');
  var booking = _.get(req, 'body');

  if (!token || !roomId || !booking || !booking.employmentContract || !booking.lastThreePaySlips || booking.lastThreePaySlips.length !== 3) {
    next(errorService.BAD_REQUEST_PARAMETERS);
  } else {
    jwt.verify(token, req.app.get('superSecret'), (err) => {
      if (err) {
        next(errorService.FORBIDDEN);
      } else {
        var clientQuery = mapper.getStatement('Client', 'select', { token: token }, db.conf);
        db.query(clientQuery, (err, clientDb) => {
          if (err) {
            next(errorService.UNEXPECTED_ERROR);
          } else if (clientDb.length == 0) {
              next(errorService.FORBIDDEN);
          } else {
            clientDb = clientDb.pop();

            var roomQuery = mapper.getStatement('Room', 'isRoomAvailable', { id: roomId });
            db.query(roomQuery, (err, roomQueryDb) => {
              if (err) {
                next(errorService.UNEXPECTED_ERROR);
              } else if (!!!roomQueryDb.length) {
                next(errorService.BAD_REQUEST_PARAMETERS);
              } else {
                var fileIds = [booking.employmentContract].concat(booking.lastThreePaySlips);
                var filesExistQuery = mapper.getStatement('File', 'doesExist', {fileIds: fileIds });
                db.query(filesExistQuery, (err, filesExistDb) => {
                  if (err) {
                    next(errorService.UNEXPECTED_ERROR);
                  } else {
                    filesExistDb = filesExistDb.pop();
    
                    if (filesExistDb.count !== fileIds.length) {
                      next(errorService.BAD_REQUEST_PARAMETERS);
                    } else {
                      booking.id = uuid.v1();
                      booking.clientId = clientDb.id;
                      booking.roomId = roomId;
          
                      var bookingQuery = mapper.getStatement('Booking', 'insert', booking);
                      db.query(bookingQuery, (err, bookingQueryDb) => {
                        if (err) {
                          next(errorService.UNEXPECTED_ERROR);
                        } else {
                          // Room is still active until booking request is accepted
                          res.json({
                            success: true,
                            message: 'Votre réservation a bien été soumise.',
                            data: booking.id
                          });
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});

module.exports = router;
