const router = require('express').Router();
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const _ = require('lodash');

mapper.createMapper(['./mappers/apartment.xml']);

// Request to get availables room
// Body : filter by city
router.get('/', function (req, res, next) {
  var requestedCity = _.get(req, 'body.city') || null;

  var roomsQuery = mapper.getStatement('Apartment', 'selectApartmentByCity', {
    city: requestedCity,
  });
  db.query(roomsQuery, (err, results) => {
    if (err) {
      next(errorService.UNEXPECTED_ERROR);
    } else {
      results.forEach(apartment => apartment.rooms = JSON.parse(apartment.rooms) );

      res.json({
        success: true,
        data: results
      });
    }
  });
});

module.exports = router;
