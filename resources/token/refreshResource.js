const router = require('express').Router({ mergeParams: true });
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

mapper.createMapper(['./mappers/client.xml']);

// Refresh token
router.post('/', function (req, res, next) {
  var client = {
    id: _.get(req.params, 'id'),
    refreshToken: _.get(req, 'body.refreshToken')
  };
  
  if (!client.id || !client.refreshToken) {
    next(errorService.BAD_REQUEST_PARAMETERS);
  } else {
    var clientQuery = mapper.getStatement('Client', 'select', {id : client.id });
    
    db.query(clientQuery, (err, clientDb) => {
      if (err) {
        next(errorService.UNEXPECTED_ERROR);
      } else {
        clientDb = clientDb.pop();

        if (!clientDb) {
          next(errorService.FORBIDDEN);
        } else if (clientDb.refreshToken !== client.refreshToken) {
          next(errorService.FORBIDDEN);
        } else {
          _.set(clientDb, 'access.token', jwt.sign({ id: clientDb.id }, req.app.get('superSecret'), { expiresIn: '15d' }));

          var updateTokenQuery = mapper.getStatement('Client', 'updateToken', clientDb);
          db.query(updateTokenQuery, (err) => {
            if (err) {
              next(errorService.UNEXPECTED_ERROR);
            } else {
              res.json({
                success: true,
                message: 'Token Refreshed !',
                data: {
                  token: clientDb.access.token,
                  expirationTime: _.get(jwt.decode(clientDb.access.token), 'exp'),
                },
              });
            }
          });
        }
      }
    });
  }
});

module.exports = router;