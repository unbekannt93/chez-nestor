const router = require('express').Router();
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcrypt');

mapper.createMapper(['./mappers/client.xml']);

// Request to authenticate
/* Body : 
    {
        email: string,
        secret: string - password
    }
*/
router.post('/', function (req, res, next) {
  var client = {
    email: _.get(req, 'body.email'),
    password: _.get(req, 'body.secret'),
  };

  if (!client.email || !client.password) {
    next(errorService.BAD_CREDENTIALS);
  } else {
    var selectQuery = mapper.getStatement('Client', 'select', client);

    db.query(selectQuery, (err, clientDb) => {
      if (err) {
        next(errorService.UNEXPECTED_ERROR);
      } else {
        if (!!clientDb.length) {
          clientDb = clientDb.pop();

          _.set(clientDb, 'access.refreshToken', clientDb.refreshToken);
          delete clientDb.refreshToken;

          comparePassword(client.password, clientDb.password, (err, match) => {
            if (!!match) {
              delete clientDb.password;
              delete clientDb.token;
              _.set(clientDb, 'access.token', jwt.sign({ id: clientDb.id }, req.app.get('superSecret'), { expiresIn: '15d' }));

              var updateTokenQuery = mapper.getStatement('Client', 'updateToken', clientDb);
              db.query(updateTokenQuery, (err) => {
                if (err) {
                  next(errorService.UNEXPECTED_ERROR);
                } else {
                  clientDb.access.expirationTime = _.get(jwt.decode(clientDb.token), 'exp');
                  
                  res.json({
                    success: true,
                    message: 'Connexion successful',
                    data: clientDb
                  });
                }
              });
            } else {
              next(errorService.BAD_CREDENTIALS);
            }
          });
        } else {
          next(errorService.BAD_CREDENTIALS);
        }
      }
    });
  }
});

var comparePassword = function (plainPass, hashword, callback) {
  bcrypt.compare(plainPass, hashword, function (err, isPasswordMatch) {
    return err == null ? callback(null, isPasswordMatch) : callback(err);
  });
};

module.exports = router;
