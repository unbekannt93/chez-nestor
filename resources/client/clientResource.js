const router = require('express').Router();
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

mapper.createMapper(['./mappers/client.xml']);

//Request to deactivate a client
router.delete('/:id', function (req, res, next) {
  var token = req.headers.baerer;
  var clientId = _.get(req.params, 'id');

  if (!token || !clientId) {
    next(errorService.BAD_REQUEST_PARAMETERS);
  } else {
    jwt.verify(token, req.app.get('superSecret'), (err) => {
      if (err) {
        next(errorService.FORBIDDEN);
      } else {
        var clientQuery = mapper.getStatement('Client', 'select', { token: token });

        db.query(clientQuery, (err, clientDb) => {
          if (err) {
            next(errorService.UNEXPECTED_ERROR);
          } else {
            clientDb = clientDb.pop();

            if (!clientDb) {
              next(errorService.FORBIDDEN);
            } else if (clientId !== clientDb.id) {
              next(errorService.FORBIDDEN);
            } else {
              var deactivateQuery = mapper.getStatement('Client', 'deactivate', { id: clientId });

              db.query(deactivateQuery, (err) => {
                if (err) {
                  next(errorService.UNEXPECTED_ERROR);
                } else {
                  res.json({
                    success: true,
                    message: 'Client deactivated successfully.'
                  });
                }
              });
            }
          }
        });
      }
    });
  }
});

module.exports = router;