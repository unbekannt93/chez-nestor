const router = require('express').Router();
const db = require('../../services/dbService');
const errorService = require('../../services/errorService');
const mapper = require('mybatis-mapper');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const randtoken = require('rand-token');
const uuid = require('uuid');

mapper.createMapper(['./mappers/client.xml']);

// Request to register
/* Body : 
    {
        email: string,
        secret: string - password,
        firstName: string,
        lastName: string,
        phone: string,
        birthDate: string - yyyy-mm-dd
        nationality: string - 2 letters code from ISO 3166 international standard (ex: France -> 'FR')
    }
*/
router.post('/', function (req, res, next) {
  var client = {
    email: _.get(req, 'body.email'),
    password: _.get(req, 'body.secret'),
    firstName: _.get(req, 'body.firstName'),
    lastName: _.get(req, 'body.lastName'),
    phone: _.get(req, 'body.phone'),
    birthDate: _.get(req, 'body.birthDate'),
    nationality: _.get(req, 'body.nationality'),
  };

  if (!client.email || !client.password || !client.firstName || !client.lastName || !client.phone || !client.birthDate || !client.nationality
  ) {
    next(errorService.BAD_REQUEST_PARAMETERS);
  } else {
    client.id = uuid.v1();
    
    var doesExistQuery = mapper.getStatement('Client', 'doesExist', client);
    db.query(doesExistQuery, (err, existsDb) => {
      existsDb = existsDb.pop();

      if (err) {
        next(errorService.UNEXPECTED_ERROR);
      } else {
        if (!!existsDb.exists) {
          next(errorService.CLIENT_ALREADY_EXIST);
        } else {
          client.access = {
            refreshToken: randtoken.uid(256)
          };

          cryptPassword(client.password, (err, hash, salt) => {
            client.password = hash;
            client.salt = salt;

            var createClientQuery = mapper.getStatement('Client', 'create', client);
            db.query(createClientQuery, (err) => {
              if (err) {
                next(errorService.UNEXPECTED_ERROR);
              } else {
                delete client.salt;
                delete client.password;
                client.access.token = jwt.sign({ id: client.id }, req.app.get('superSecret'), { expiresIn: '15d' });

                var updateTokenQuery = mapper.getStatement('Client', 'updateToken', client);
                db.query(updateTokenQuery, (err) => {
                  if (err) {
                    next(errorService.UNEXPECTED_ERROR);
                  } else {
                    client.access.expirationTime = _.get(jwt.decode(client.token), 'exp');

                    res.json({
                      success: true,
                      message: 'Registration successful',
                      data: client
                    });
                  }
                });
              }
            });
          });
        }
      }
    });
  }
});

var cryptPassword = function (password, callback) {
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
      return callback(err);
    }

    bcrypt.hash(password, salt, function (err, hash) {
      return callback(err, hash, salt);
    });
  });
};

module.exports = router;
