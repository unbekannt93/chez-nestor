const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const db = require('../services/dbService');
const mapper = require('mybatis-mapper');
const _ = require('lodash');
const should = chai.should();
chai.use(chaiHttp);

mapper.createMapper(['./mappers/booking.xml']);

// Testing barer token
var bearerToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFkZWM1NjcwLWFjYjAtMTFlYS05MmFiLTMzZTNjODY2ZTAyOCIsImlhdCI6MTU5MjA3NzY2NSwiZXhwIjoxNTkzMzczNjY1fQ.9qj5Zp6abA-34EJW16-63Uzf_vnh9rpKSUTzZOyL_ic';

// Testing room
var id = '2e6d3a30-aa4f-11ea-aec4-419131dca3ba';

// Testing employment contract file id
var employmentContractFileId = '02b49970-ad5f-11ea-b68a-a30344c1b964';

// Testing last three pay slips
var lastThreePaySlipsFileIds = [
  '41cc83a0-ad5c-11ea-a7a4-097d931608f0',
  '41cc83a1-ad5c-11ea-a7a4-097d931608f0',
  '4589d9c0-ad5c-11ea-a7a4-097d931608f0'
]

describe('Booking Testing', () => {
  it('/POST Booking - it should book a room', (done) => {
    chai.request(app)
    .post(`/public/room/${id}/book`)
    .set({ baerer: bearerToken })
    .send({
      employmentContract: employmentContractFileId,
      lastThreePaySlips: lastThreePaySlipsFileIds
    })
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      
      var createdBookingRequest = _.get(res, 'body.data');
      var deleteBookingRequestQuery = mapper.getStatement('Booking', 'delete', { id: createdBookingRequest });
      db.query(deleteBookingRequestQuery, () => {
        done();
      });
    });
  });
});