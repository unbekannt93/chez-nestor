const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const _ = require('lodash');
const db = require('../services/dbService');
const mapper = require('mybatis-mapper');
chai.use(chaiHttp);

mapper.createMapper(['./mappers/client.xml']);

describe('Client Testing', () => {
  var createdClient;

  it('/POST Registration - it should create a new client', (done) => {
    chai.request(app)
    .post('/public/client/register')
    .send({
      'email': 'test@test.com',
      'secret': 'test123',
      'firstName': 'Firstname',
      'lastName': 'Lastname',
      'phone': '06.60.66.00.01',
      'birthDate': '1994-07-11',
      'nationality': 'FR'
    })
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      
      createdClient = res.body.data;
      done();
    });
  });

  it('POST Authenticate - it should authenticate a client', (done) => {
    chai.request(app)
    .post('/public/client/authenticate')
    .send({
      'email': 'test@test.com',
      'secret': 'test123'
    })
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      
      done();
    });
  });

  it('POST Refresh Token - it should refresh client\'s token', (done) => {
    chai.request(app)
    .post(`/public/client/${createdClient.id}/refresh-token`)
    .send({
      'refreshToken': createdClient.access.refreshToken
    })
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      
      createdClient.access.token = _.get(res, 'body.data.token');
      done();
    });
  });

  it('DELETE - it should deactivate the client', (done) => {
    chai.request(app)
    .delete(`/public/client/${createdClient.id}`)
    .set({ baerer: _.get(createdClient, 'access.token') })
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');

      // Remove test client from db;
      var deleteClientQuery = mapper.getStatement('Client', 'delete', { id: createdClient.id });
      db.query(deleteClientQuery, () => {        
        done();
      });  
    });
  });
});