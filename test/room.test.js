const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const should = chai.should();
chai.use(chaiHttp);

describe('Room Testing', () => {
  it('/GET Rooms - it should returns available rooms', (done) => {
    chai.request(app)
    .get('/public/rooms')
    .end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      
      done();
    });
  });
});