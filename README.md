
# Chez Nestor - Test technique : développement d'une API Rest

  

# Environnement technique 

- développement du serveur en NodeJs

- base de données MySql (ClearDb)

- Heroku pour le déploiement de l'application

  

# Dépendances 

- **body-parser** : parse les requêtes
- **express** : serveur HTTP
- **lodash** : librairie de fonctions courantes
- **morgan** : logger
- **mybatis-mapper** : outils permettant d'utiliser des déclarations de mappage, et de garder un code propre en séparant les queries du reste du code (.xml)
- **mysql** : serveur SQL pour le traitement des requêtes avec la base de données
- **bcrypt** : hash password
- **multer** : gère les fichiers multi-parties
- **rand-token** : génère des tokens
- **uuid** : génère des id (J'ai utilisé la méthode V1() qui se base les timestamps)

C'est la première fois que je déploie sur heroku, et j'ai donc été limité à leur version de base de données, et malheureusement certaines fonctions d’agrégation ne sont pas disponibles.

# Fonctionnement
L'utilisateur peur lister les appartements disponibles, en filtrant par ville.
L'utilisateur s'intéresse à louer un bien, il crée donc son compte.
Un token d'accès lui est attribué, qui doit être regénéré en cas d'expiration.
Il a également la possibilité de supprimer son compte (désactivation en base).

Lorsqu'il souhaite faire une demande de réservation, il doit télécharger vers le serveur son dossier, à savoir :
* son contrat de travail
* ses trois derniers bulletin de salaire

Il peut ensuite soumettre sa demande, et son dossier passe en statut 'En cours de traitement'.
Sa demande peut également prendre un des statuts suivant : _Annulé_, _Approuvé_, _Rejeté_.

# Routes
**Client**

**/public/client/register** - *Méthode Post* - Permet de créer un compte
Body :
```
{
	"email": "test@test.com",
	"secret": "test123",
	"firstName": "Damien",
	"lastName": "Lohmeyer",
	"phone": "06.29.17.15.08",
	"birthDate": "1994-07-11",
	"nationality": "FR"
}
```
Réponse :
```
{
	"success": true,
	"message": "Registration successful",
	"data": {
		"email": "test@test.com",
		"firstName": "Damien",
		"lastName": "Lohmeyer",
		"phone": "06.29.17.15.08",
		"birthDate": "1994-07-11",
		"nationality": "FR",
		"id": "46ce15c0-adad-11ea-b345-51c559c62189",
		"access": {
			"refreshToken": "qfdvxexAIUt3uRMedz6aD4KcxaOspCJSroooFzlPABxMLj6u6QzgjJa78FpDlJPFvrrXhGD9acvfQ8nf8taKRRQq4Y6kttDwu3V42Cpu4suIqPhnajJPQqgTk9jWrZ5iLPe2Ydoov6LVKsCNK4wtsxVcMUnq2IN9AF7FiCsSr7Mroyfxu6MrqlZg4ONPK2yN5TtaBa3hL7JJ5qVxcvONAEDirOB6YceXrNEaCIWpTxKxCyW3XyLDbCI3T7BloLKH",
			"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjQ2Y2UxNWMwLWFkYWQtMTFlYS1iMzQ1LTUxYzU1OWM2MjE4OSIsImlhdCI6MTU5MjA3NzAzMiwiZXhwIjoxNTkzMzczMDMyfQ.OFL8yLO9DDEmMwWg_elDiwWDkAsIX_XV9X3yCnM8SYg"
		}
	}
}
```
#
**/public/client/authenticate** - *Méthode POST - Permet de s'authentifier et d'obtenir un nouveau token
Body :
```
{
	"email": "damienlohmeyer+test@gmail.com",
	"secret": "test123"
}
```
Réponse :
```
{
	"success": true,
	"status": 200,
	"message": "Connexion successful",
	"data": {
		"id": "adec5670-acb0-11ea-92ab-33e3c866e028",
		"firstName": "Damien",
		"lastName": "Lohmeyer",
		"email": "damienlohmeyer+test@gmail.com",
		"phone": "06.29.17.15.08",
		"birthDate": "1994-07-11",
		"nationality": "FR",
		"access": {
			"refreshToken": "mRMSanBMeJGAL9u7jVBmjcBsWJKP6oeyUaQBNcgKcUFgG4PjD6QjIst1MndaoaFborKbOzdcm9ReVt9pi2jNZsjZ5Q93txTRpAnNqKZsHhtcahVfazfMwwFG6EI08dHuLoPP5oN4VAq8P8GfYUSbnyhFz302CYkwTKNQRz7Lqqh7rLMoR3twQNIv6DQKX6foQKM0JmUR1blgLqnf8H3ShK4mVpArZz8v3rlm8WhFJQ8NkX3KwqjIVID5gLRLbxpo",
			"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFkZWM1NjcwLWFjYjAtMTFlYS05MmFiLTMzZTNjODY2ZTAyOCIsImlhdCI6MTU5MjA3NzM3MywiZXhwIjoxNTkzMzczMzczfQ.J9ZAPE-1EROQtJ3RuGSvyQoahQiPsO7rTZlH4yehuuo"
		}
	}
}
```
#
**/public/client/:id/refresh-token** - *Méthode POST - Permet de générer un nouveau token
**Path Variable** : id du client
Body :
```
{
	"refreshToken": "mRMSanBMeJGAL9u7jVBmjcBsWJKP6oeyUaQBNcgKcUFgG4PjD6QjIst1MndaoaFborKbOzdcm9ReVt9pi2jNZsjZ5Q93txTRpAnNqKZsHhtcahVfazfMwwFG6EI08dHuLoPP5oN4VAq8P8GfYUSbnyhFz302CYkwTKNQRz7Lqqh7rLMoR3twQNIv6DQKX6foQKM0JmUR1blgLqnf8H3ShK4mVpArZz8v3rlm8WhFJQ8NkX3KwqjIVID5gLRLbxpo"
}
```
Réponse :
```
{
	"success": true,
	"message": "Token Refreshed !",
	"data": {
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFkZWM1NjcwLWFjYjAtMTFlYS05MmFiLTMzZTNjODY2ZTAyOCIsImlhdCI6MTU5MjA2NjkwNSwiZXhwIjoxNTkzMzYyOTA1fQ.4SfhV2zI5GtVZAmcRj42hWQVmKISPJHMB3ZiF3dkhW4",
		"expirationTime": 1593362905
	}
}
```
#
**/public/client/:id** - *Méthode DELETE- Permet de désactiver le compte d'un client
**Path Variable** : id du client
Réponse :
```
{
	"success": true,
	"message": "Client deactivated successfully."
}
```
#
**File**
**/public/file** - *Méthode POST* - Permet d'upload un fichier
**/public/files** - *Méthode POST* - Permet d'uplaod plusieurs fichiers
Body : multipart - key : file / files depending on the route
Réponse : tableau d'id des fichiers générées
```
{
	"success": true,
	"status": 200,
	"data": [
		"02b49970-ad5f-11ea-b68a-a30344c1b964"
	]
}
```
#
**Room**
**/public/rooms** - *Méthode GET- Permet d'obtenir la liste des chambres disponibles
Body - *Optionnel*:
```
{
	"city": "lyon"
}
```
Réponse : soit un tableau vide, soit :
```
{
	"success": true,
	"status": 200,
	"data": [
		{
			"id": "e70737fd0-aa4e-11ea-b790-7b27e2bf9e31",
			"name": "17 Récamier",
			"street": "17 rue Juliette Récamier",
			"zipcode": "6900",
			"city": "Lyon",
			"rooms": [
				{
					"id": "2e6d3a30-aa4f-11ea-aec4-419131dca3ba",
					"number": 3,
					"area": 9,
					"price": 41700
				}
			]
		}
	]
}
```
#
**Booking**
**/public/room/:id/book** - *Méthode POST - Permet d'effectuer une demande de réservation
**Path Variable** : id de la chambre
Body :
```
{
	"employmentContract": "4589d9c0-ad5c-11ea-a7a4-097d931608f0",
	"lastThreePaySlips": [
		"41cc83a0-ad5c-11ea-a7a4-097d931608f0",
		"41cc83a1-ad5c-11ea-a7a4-097d931608f0",
		"02b49970-ad5f-11ea-b68a-a30344c1b964"
	]
}
```
Réponse : retourne l'id de la réservation
```
{
	"success": true,
	"message": "Votre réservation a bien été soumise.",
	"data": "90460a20-adb0-11ea-93da-17ec6cc70b95"
}
```
# Base de données
J'ai créé ma structure de base de données avec ces requêtes :

*Création de la table Apartment :*

```sql
CREATE  TABLE  IF  NOT  EXISTS apartment (
	id varchar(40) NOT  NULL  PRIMARY  KEY,
	name  varchar(255) NOT  NULL,
	street text  NOT  NULL,
	zipcode varchar(16) NOT  NULL,
	city varchar(255) NOT  NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
#
*Création de la table Room :*
```sql
CREATE TABLE IF NOT EXISTS `room` (
	id varchar(40) NOT NULL PRIMARY KEY,
	number INT NOT NULL,
	area FLOAT NOT NULL,
	price INT NOT NULL,
	apartment_id varchar(40) NOT NULL,
	active BOOLEAN DEFAULT true,
	FOREIGN KEY (apartment_id) REFERENCES apartment(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
J'ai ajouté un boolean qui permet de connaitre l'état de disponibilité de la chambre
#
*Création de la table Client :*
```sql
CREATE TABLE IF NOT EXISTS `client` (
	id varchar(40) NOT NULL PRIMARY KEY,
	first_name varchar(50) NOT NULL,
	last_name varchar(50) NOT NULL,
	email varchar(62) NOT NULL,
	phone varchar(20) NOT NULL,
	birth_date DATE NOT NULL,
	nationality varchar(2) NOT NULL,
	password varchar(255) NOT NULL,
	salt varchar(255) NOT NULL,
	token varchar(256),
	refresh_token varchar(256) NOT NULL,
	active BOOLEAN DEFAULT true
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
J'ai ajouté : 
* password : un mot de passe
* salt : base de hash du mot de passe
* token : un token pour vérifier les droits d'accès aux ressources
* active : un boolean qui permet de connaitre le statut d'un client (par exemple s'il a désactivé son compte, s'il a été banni, etc
#
*Création de la table File:*
Pour stocker les pièces justificatives nécessaire à une demande de réservation
```sql
CREATE TABLE IF NOT EXISTS `file` (
	id varchar(40) NOT NULL PRIMARY KEY,
	client_id varchar(40) NOT NULL,
	original_name varchar(255),
	mime_type varchar(20) NOT NULL,
	creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (client_id) REFERENCES client(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
#
*Création de la table Booking Request Status:*
Permet de stocker les statuts et de mettre une foreign key sur la table booking_request
```sql
CREATE TABLE IF NOT EXISTS `booking_request_status` (
	id varchar(20) NOT NULL PRIMARY KEY
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
#
*Création de la table Booking Request:*
Toutes les demandes de réservations sont stockées dans cette table
```sql
CREATE TABLE IF NOT EXISTS `booking_request` (
	id varchar(40) NOT NULL PRIMARY KEY,
	status varchar(20) NOT NULL DEFAULT 'IN_PROGRESS',
	room_id varchar(40) NOT NULL,
	client_id varchar(40) NOT NULL,
	employment_contract varchar(40) NOT NULL,
	last_three_pay_slips LONGTEXT NOT NULL,
	FOREIGN KEY (status) REFERENCES booking_request_status(id),
	FOREIGN KEY (room_id) REFERENCES room(id),
	FOREIGN KEY (client_id) REFERENCES client(id),
	FOREIGN KEY (employment_contract) REFERENCES file(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
Elle lie un client avec une chambre.
Elle stocke différentes pièces justificatives nécessaire à l'étude de la demande.
#
Concernant les tailles de stockages, j'ai repris les standards que j'ai trouvé sur internet. 

J'ai ensuite manuellement rempli la base avec des données de test :
*Les différents statuts de demande de réservation :*
```sql
	INSERT INTO booking_request_status (id)
	VALUES ('CANCELED'), ('IN_PROGRESS'), ('APPROVED'), ('REJECTED');
```

*Quelques appartements :*

```sql
INSERT  INTO
	apartment (id, name, street, zipcode, city)
VALUES
	('e7c91460-aa4d-11ea-bcdb-498ccfd82a99', '220 Guillotiere', '220 Grande rue de la Guillotière', '69007', 'Lyon'),
	('e70737fd0-aa4e-11ea-b790-7b27e2bf9e31', '17 Récamier', '17 rue Juliette Récamier', '6900', 'Lyon');
```
*Quelques chambres :*
```sql
INSERT  INTO
	room (id, number, area, price, apartment_id)
VALUES
	('1c47b060-aa4f-11ea-ba5a-01c3c9e0b437', 3, 9, 41500, 'e7c91460-aa4d-11ea-bcdb-498ccfd82a99'),
	('2e6d3a30-aa4f-11ea-aec4-419131dca3ba', 3, 9, 41700, 'e70737fd0-aa4e-11ea-b790-7b27e2bf9e31'),
	('b013cc80-aa4e-11ea-99dd-6588e895fb23', 2, 9, 43100, 'e7c91460-aa4d-11ea-bcdb-498ccfd82a99');
```
*Un client :*
```sql
INSERT  INTO
	client (id, first_name, last_name, email, phone, birth_date, nationality)
VALUES
	('040066b0-aa67-11ea-95a7-b95c448a41a7', 'Damien', 'Lohmeyer', 'damienlohmeyer@gmail.com', '06.29.17.15.08', '1994-07-11', 'Française');
```
# Tests unitaires
Il y a trois fichiers de tests unitaires définis :
* **client** : teste la création d'un compte, la connexion, la regénération du token, ainsi que la désactivation du compte
* **room**: vérifie que la route permettant de récupérer les chambres disponible est fonctionnelle
*  **booking**: effectue une demande de réservation

J'ai volontaire séparé les trois fichiers, pour plus de clarté.
J'ai par conséquence crée demande de réservation en utilisant les données de tests d'un utilisateur en base, pour éviter d'avoir des dépendances avec le fichier de test du client (de cette façon, si la création d'un compte échoue, je peux quand même tester la demande de réservation.

**Commande pour lancer les tests**: *npm run test*
**Résultats :**
```
Booking Testing
- "POST /public/room/2e6d3a30-aa4f-11ea-aec4-419131dca3ba/book HTTP/1.1" 200 128
    √ /POST Booking - it should book a room (1543ms)


Client Testing
- "POST /public/client/register HTTP/1.1" 200 727
    √ /POST Registration - it should create a new client (721ms)

- "POST /public/client/authenticate HTTP/1.1" 200 724
    √ POST Authenticate - it should authenticate a client (497ms)

- "POST /public/client/b8185340-adb1-11ea-a196-9dd97a269b40/refresh-token HTTP/1.1" 200 281
    √ POST Refresh Token - it should refresh client's token (427ms)

- "DELETE /public/client/b8185340-adb1-11ea-a196-9dd97a269b40 HTTP/1.1" 200 61
    √ DELETE - it should deactivate the client (622ms)


Room Testing
- "GET /public/rooms HTTP/1.1" 200 583
    √ /GET Rooms - it should returns available rooms (219ms)


  6 passing (4s)

```