var http = require('http');

var errors = {
    BAD_REQUEST_PARAMETERS: {status: 400, message: http.STATUS_CODES['400'] },
    BAD_CREDENTIALS: {status: 401, message: http.STATUS_CODES['401'] },
    NEED_IDENTIFICATION: {status: 401, message: http.STATUS_CODES['401'] },
    CLIENT_ALREADY_EXIST: {status: 409, message: http.STATUS_CODES['409'] },
    FORBIDDEN: {status: 403, message: http.STATUS_CODES['403'] },
    NOT_FOUND: {status: 404, message: http.STATUS_CODES['404'] },
    UNEXPECTED_ERROR: {status: 500, message: http.STATUS_CODES['500']}
};

module.exports = errors;