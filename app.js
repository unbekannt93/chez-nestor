const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');
const config = require('./config/config');
const multer  = require('multer');

var upload = multer({ dest: config.uploadDirectory });

const PORT = process.env.PORT || config.port;

const app = express();
const command = express.Router({ mergeParams: true });

app.set('superSecret', config.secret);

app.all('*', function (req, res, next) {
  var origin = req.get('origin');
  res.header('Access-Control-Allow-Origin', origin);
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  res.header('Content-Type', 'application/json');
  next();
});

// view engine setup
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// Set up logger
app.use(logger('common'));

// Set up json parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/*------------------------ Routes definition ------------------------*/
// User
app.use('/public/client', require('./resources/client/clientResource'));
app.use('/public/client/register', require('./resources/client/registrationResource'));
app.use('/public/client/authenticate', require('./resources/client/authenticationResource'));
// Token
app.use('/public/client/:id/refresh-token', require('./resources/token/refreshResource'));

// Rooms
app.use('/public/rooms', require('./resources/room/roomsResource'));
app.use('/public/room/:id/book', require('./resources/room/bookingResource'));

// File
app.use('/public/file', upload.single('file'), require('./resources/file/filesResource'));
app.use('/public/files', upload.array('files'), require('./resources/file/filesResource'));
/*-------------------------------------------------------------------*/

// 404 Error forwarding
app.use(function (req, res, next) {
  res.status(404);
  res.header('Content-Type', 'text/html');
  res.render('404_error');
  next();
});

// Error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({ error: err.message || 'Unexpected Error' });
  next();
});

app.listen(PORT, () => {
  console.log('Chez-Nestor API is ready !');
});

module.exports = app;