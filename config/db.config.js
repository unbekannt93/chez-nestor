module.exports = {
    HOST: 'us-cdbr-east-05.cleardb.net',
    USER: 'bce7267742c228',
    PASSWORD: '57260293',
    DB: 'heroku_155f00b964c40eb'
  };

  //mysql --host=us-cdbr-east-05.cleardb.net --user=bce7267742c228 --password=57260293 --reconnect heroku_155f00b964c40eb

  //CREATE TABLE IF NOT EXISTS `apartment` (id varchar(40) NOT NULL PRIMARY KEY, name varchar(255) NOT NULL, street text NOT NULL, zipcode varchar(16) NOT NULL, city varchar(255) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  //CREATE TABLE IF NOT EXISTS `room` (id varchar(40) NOT NULL PRIMARY KEY, number INT NOT NULL, area FLOAT NOT NULL, price INT NOT NULL, apartment_id varchar(40) NOT NULL, active BOOLEAN DEFAULT true, FOREIGN KEY (apartment_id) REFERENCES apartment(id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  //CREATE TABLE IF NOT EXISTS `client` (id varchar(40) NOT NULL PRIMARY KEY, first_name varchar(50) NOT NULL, last_name varchar(50) NOT NULL, email varchar(62) NOT NULL, phone varchar(20) NOT NULL, birth_date DATE NOT NULL, nationality varchar(2) NOT NULL, password varchar(255) NOT NULL, salt varchar(255) NOT NULL, token varchar(256), refresh_token varchar(256) NOT NULL, active BOOLEAN DEFAULT true) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  //CREATE TABLE IF NOT EXISTS `file` (id varchar(40) NOT NULL PRIMARY KEY, client_id varchar(40) NOT NULL, original_name varchar(255), mime_type varchar(20) NOT NULL, creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (client_id) REFERENCES client(id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  //CREATE TABLE IF NOT EXISTS `booking_request_status` (id varchar(20) NOT NULL PRIMARY KEY) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  //CREATE TABLE IF NOT EXISTS `booking_request` (id varchar(40) NOT NULL PRIMARY KEY, status varchar(20) NOT NULL DEFAULT 'IN_PROGRESS', room_id varchar(40) NOT NULL, client_id varchar(40) NOT NULL, employment_contract varchar(40) NOT NULL, last_three_pay_slips LONGTEXT NOT NULL, FOREIGN KEY (status) REFERENCES booking_request_status(id), FOREIGN KEY (room_id) REFERENCES room(id), FOREIGN KEY (client_id) REFERENCES client(id), FOREIGN KEY (employment_contract) REFERENCES file(id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  // Manually populating some data for testing purpose
  // Booking status
  //INSERT INTO booking_request_status (id) VALUES ('CANCELED'), ('IN_PROGRESS'), ('APPROVED'), ('REJECTED');

  // Some apartments
  //INSERT INTO apartment (id, name, street, zipcode, city) VALUES ('e7c91460-aa4d-11ea-bcdb-498ccfd82a99', '220 Guillotiere', '220 Grande rue de la Guillotière', '69007', 'Lyon');
  //INSERT INTO apartment (id, name, street, zipcode, city) VALUES ('e70737fd0-aa4e-11ea-b790-7b27e2bf9e31', '17 Récamier', '17 rue Juliette Récamier', '6900', 'Lyon');

  // Some rooms
  //INSERT INTO room (id, number, area, price, apartment_id) VALUES ('1c47b060-aa4f-11ea-ba5a-01c3c9e0b437', 3, 9, 41500, 'e7c91460-aa4d-11ea-bcdb-498ccfd82a99');
  //INSERT INTO room (id, number, area, price, apartment_id) VALUES ('2e6d3a30-aa4f-11ea-aec4-419131dca3ba', 3, 9, 41700, 'e70737fd0-aa4e-11ea-b790-7b27e2bf9e31');
  //INSERT INTO room (id, number, area, price, apartment_id) VALUES ('b013cc80-aa4e-11ea-99dd-6588e895fb23', 2, 9, 43100, 'e7c91460-aa4d-11ea-bcdb-498ccfd82a99');

 